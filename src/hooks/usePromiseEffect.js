import { useState, useEffect } from 'react';

const usePromiseEffect = (promise, dependencies = [], defaultResult) => {
  const [isLoading, setIsLoading] = useState(false);
  const [result, setResult] = useState(defaultResult);
  const [error, setError] = useState(undefined);

  const request = async () => {
    setIsLoading(true);

    try {
      const result = await promise();

      setResult(result);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    request();
  }, dependencies);

  return [isLoading, result, error];
};

export default usePromiseEffect;
