import { useState, useCallback } from 'react';

const useTextInput = label => {
  const [value, setValue] = useState('');
  const onChangeText = useCallback(text => {
    setValue(text);
  });

  return {
    onChangeText,
    label,
    value,
  };
};

export default useTextInput;
