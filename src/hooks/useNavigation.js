import { useContext } from 'react';
import { NavigationContext } from '@react-navigation/core';

const useNavigation = () => {
  return useContext(NavigationContext);
};

export default useNavigation;
