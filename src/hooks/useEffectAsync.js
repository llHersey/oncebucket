import { useEffect } from 'react';

const useEffectAsync = (cb, dependencies) => {
  useEffect(() => {
    cb();
  }, dependencies);
};

export default useEffectAsync;
