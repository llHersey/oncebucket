import { useState } from 'react';

const usePromise = promise => {
  const [isLoading, setIsLoading] = useState(false);
  const [result, setResult] = useState();
  const [error, setError] = useState();

  const request = async () => {
    setIsLoading(true);
    try {
      const result = await promise();

      setResult(result);
    } catch (error) {
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  return [isLoading, result, request, error];
};

export default usePromise;
