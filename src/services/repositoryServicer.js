import api from '../api';

const getRepositories = async user => {
  const response = await api.get(`/repositories/${user}`);

  return response.data;
};

export { getRepositories };
