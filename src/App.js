import React from 'react';
import { Provider as ThemeProvider } from 'react-native-paper';
import Navigation from './navigation';
import Theme from './theme';

const App = () => (
  <ThemeProvider theme={Theme}>
    <Navigation />
  </ThemeProvider>
);

export default App;
