import {
  createAppContainer,
  createBottomTabNavigator,
  createStackNavigator,
} from 'react-navigation';
import Repositories from '../screens/repositories';
import Profile from '../screens/profile';
import Login from '../screens/login';

const navigation = createStackNavigator(
  {
    Login: {
      screen: Login,
    },
    Content: createBottomTabNavigator({
      Repositories: {
        screen: Repositories,
      },
      Profile: {
        screen: Profile,
      },
    }),
  },
  {
    headerMode: 'none',
  }
);

export default createAppContainer(navigation);
