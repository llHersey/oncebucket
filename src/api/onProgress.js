class Progress {
  handlers = [];

  startTracking(handler) {
    this.handlers.push(handler);
  }

  endTracking() {
    this.handlers = [];
  }

  notify(data) {
    this.handlers.slice(0).forEach(cb => cb(data));
  }

  complete() {
    this.handlers = [];
  }
}

export default new Progress();
