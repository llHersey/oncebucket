import axios from 'axios';
import tools from './tools';
import onProgress from './onProgress';
import logger from '../log';

let cancelers = [];

const cancelRequest = () => {
  cancelers.forEach(c => c());
  cancelers = [];
};

const instance = axios.create({
  baseURL: 'https://api.bitbucket.org/2.0',
  onUploadProgress: event => {
    onProgress.notify({
      percentage: Math.round((event.loaded * 100) / event.total),
      total: event.total,
    });

    if (event.loaded === event.total) onProgress.complete();
  },
});

const handleError = error => {
  if (axios.isCancel(error)) {
    return Promise.reject({ response: { data: 'Cancelled by user' } });
  }

  if (!tools.isExpected(error)) {
    logger.log(error);
  }
  return Promise.reject(error);
};

instance.interceptors.request.use(
  request => ({
    ...request,
    cancelToken: new axios.CancelToken(c => {
      cancelers.push(c);
    }),
  }),
  error => handleError(error)
);

instance.interceptors.response.use(response => response, error => handleError(error));

const toQueryString = obj => {
  const parts = [];

  for (const property in obj) {
    if (property && obj.hasOwnProperty(property)) {
      const value = obj[property];

      if (value) parts.push(`${encodeURIComponent(property)}=${encodeURIComponent(value)}`);
    }
  }
  return parts.join('&');
};

const setJWT = token => {
  instance.defaults.headers.common['Authorization'] = `bearer ${token}`;
};

export default {
  cancelRequest,
  delete: instance.delete,
  get: instance.get,
  isCancel: axios.isCancel,
  post: instance.post,
  put: instance.put,
  toQuery: toQueryString,
  setJWT,
};
