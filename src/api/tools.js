const isNotFound = err => err.response && err.response.status === 404;
const IsUnauthorized = err => err.response && err.response.status === 401;
const isExpected = err => err.response && err.response.status >= 400 && err.response.status < 500;

export default {
  isExpected,
  isNotFound,
  IsUnauthorized,
};
