import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import { useTextInput, useNavigation } from '../../hooks';

const Login = () => {
  const username = useTextInput('Username');
  const pass = useTextInput('Password');
  const navigation = useNavigation();

  const tryLogin = useCallback(() => {
    navigation.navigate('Content');
  });

  return (
    <View style={styles.container}>
      <View style={styles.header} />
      <View style={styles.inputContainer}>
        <TextInput {...username} type="outlined" style={styles.input} />
        <TextInput {...pass} type="outlined" style={styles.input} secureTextEntry />
        <Button mode="contained" style={styles.input} onPress={tryLogin}>
          Login
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: '50%',
    backgroundColor: '#0747a6',
  },
  inputContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  input: {
    width: 350,
    margin: 20,
  },
});

export default Login;
