import React from 'react';
import { StyleSheet, View, Image } from 'react-native';

const Profile = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image
          style={styles.profilePhoto}
          source={{
            uri:
              'https://secure.gravatar.com/avatar/c575f34e66a1826b031825a3fa18946a?r=g&d=https' +
              '://avatar-cdn.atlassian.com/default/96&s=512',
          }}
        />
      </View>
      <View style={styles.content} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#0747a6',
    height: 100,
    justifyContent: 'flex-end',
  },
  content: {
    flex: 3,
  },
  profilePhoto: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignItems: 'flex-end',
    marginLeft: 20,
    transform: [{ translateY: 50 }],
  },
});

export default Profile;
