import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { usePromiseEffect } from '../../hooks';
import { getRepositories } from '../../services/repositoryServicer';
import RepositoryCard from './repositoryCard/repositoryCard';
import { FlatList } from 'react-native-gesture-handler';

const Repositories = () => {
  const [isLoading, data = { values: [] }] = usePromiseEffect(() => getRepositories('asphyo'), []);
  const keyExtractor = useCallback(item => item.uuid);
  const renderItem = useCallback(({ item }) => <RepositoryCard repo={item} />);

  return (
    !isLoading && (
      <FlatList
        style={styles.container}
        data={data.values}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
    )
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f7f7',
  },
});

export default Repositories;
