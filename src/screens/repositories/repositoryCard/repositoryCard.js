import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const RepositoryCard = ({ repo }) => (
  <View style={styles.repo} key={repo.uuid}>
    <View style={styles.iconContainer}>
      <Image source={{ uri: repo.links.avatar.href }} style={styles.icon} />
    </View>
    <View style={styles.separator} />
    <View style={styles.textContainer}>
      <Text style={styles.title}>{repo.name}</Text>
      {!!repo.language && (
        <Text style={styles.subTitle}>
          {repo.language[0].toUpperCase() + repo.language.slice(1)}
        </Text>
      )}
      {!!repo.description && <Text style={styles.subTitle}>{repo.description}</Text>}
    </View>
  </View>
);

const styles = StyleSheet.create({
  iconContainer: {
    marginLeft: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  icon: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
  repo: {
    height: 70,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 7,
    backgroundColor: '#fff',
    flexDirection: 'row',
  },
  separator: {
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: '#eaeceb',
    margin: 15,
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#878988',
  },
  subTitle: {
    fontSize: 12,
    fontWeight: 'bold',
    color: '#878988',
  },
  textContainer: {
    justifyContent: 'center',
  },
});

export default RepositoryCard;
